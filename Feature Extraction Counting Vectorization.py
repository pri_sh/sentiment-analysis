from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd
import pickle
import numpy as np
import h5py

with open('Data_After_Clean.pkl', 'rb') as f:
    mynewlist = pickle.load(f)


Tweets = []
for i in range(len(mynewlist)):
    Tweets.append(' '.join(mynewlist[i]))


vectorizer = CountVectorizer()
vector = vectorizer.fit_transform(Tweets)
A = vector.toarray()


np.savez_compressed('Count_Compressed', a=A)

#loaded = np.load('Count_Compressed.npz')
#A = loaded['a']

