import pandas as pd
import nltk
import re
import string
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer 
import pickle
#######################################################################################

def Remove_punctuation(sent):
    punc_free = ''.join([i for  i in sent if i not in string.punctuation])
    return punc_free

def Remove_stop_words(sent):
    sent = [i for i in sent if i not in stopwords]
    return sent
    
def Lemmitization(sent):
    lemmatizer = WordNetLemmatizer()  
    sent = [lemmatizer.lemmatize(i) for i in sent]    
    return sent

def Clean(sentence_data):
    sentence_data = sentence_data.lower()
    sentence_data = re.sub('<.*?>', '', sentence_data)   # remove HTML tags
    sentence_data = re.sub(r"@(\w+)", "", sentence_data)
    sentence_data = re.sub(r'http\S+', '', sentence_data)
    sentence_data = re.sub(r'[0-9]', '', sentence_data)
    sentence_data = Remove_punctuation(sentence_data)
    sentence_data = re.sub(r"[#|_]", '', sentence_data) # remove the # in #hashtag
    sentence_data = nltk.word_tokenize(sentence_data)
    lemmatizer = WordNetLemmatizer()  
    sentence_data = Lemmitization(sentence_data)
    sentence_data = Remove_stop_words(sentence_data)
    
    for i in range(len(sentence_data)-1,-1, -1):
        if len(sentence_data[i])<=2:
            sentence_data.remove(sentence_data[i])
    
    
    
    
    return sentence_data


nltk.download('punkt')
nltk.download('stopwords')
stopwords = nltk.corpus.stopwords.words('english')
nltk.download('wordnet')
DS_COLUMNS  = ["sentiment", "text"]

Label = [0, 1]

All_Data = []
All_Label = []
for j in Label:
    name = 'Label_'+str(j)+'.csv'

    df = pd.read_csv(name, names=DS_COLUMNS)

    data = df.loc[:, 'text']
    
    Data = []
    Label_Data = []
    for i in range(data.shape[0]):
        sentence_data = data.iloc[i]    
        Cleaned_Sentence = Clean(sentence_data)
        if len(Cleaned_Sentence)>0:
            Data.append(Cleaned_Sentence)
            Label_Data.append(j)
    
    All_Data.append(Data)
    All_Label.append(Label_Data)
    



mylist = All_Data[0]+All_Data[1]

with open('Data_After_Clean_Without_Stem.pkl', 'wb') as f:
     pickle.dump(mylist, f)

mylabel = All_Label[0]+All_Label[1]
pd.DataFrame(mylabel).to_csv('Labels.csv')




































