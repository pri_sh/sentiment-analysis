import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import confusion_matrix
from sklearn.metrics import RocCurveDisplay
import matplotlib.pyplot as plt
from sklearn.metrics import auc
import pandas as pd
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.svm import SVC




y = pd.read_csv('Labels.csv').values
y = y[:99482]
y0 = y[y==0]
y0 = -1*np.ones(y0.shape[0])
y1 = y[y==1]
y3 = np.concatenate([y0, y1], axis = 0)
y = y3

Cost = [1, 2, 4, 8, 16]
L = [10, 20, 40, 80, 160]
for l in L:
    for k in Cost:
        name2 = 'Hashing_Vecotorize_'+str(l)+'.npy'    
        X = np.load(name2)
        
        
        kfold = 10
        n_samples, n_features = X.shape
        cv = StratifiedKFold(n_splits=kfold)
        
        tprs = []
        aucs = []
        mean_fpr = np.linspace(0, 1, 100)
        Performance_criteria = []
        
        k_neighbour = k
        Hashing = l
        name = 'SVM_'+str(k_neighbour)+'_'+str(Hashing)+'.png'
        name1 = 'SVM_'+str(k_neighbour)+'_'+str(Hashing)+'.csv'
        
        fig, ax = plt.subplots()
        for i, (train, test) in enumerate(cv.split(X, y)):
        
            print(i)
            clf = SVC(kernel='linear', C=k)
            clf.fit(X[train], y[train])
            clf.predict_proba(X[test])
            y_pred = clf.predict(X[test])
            
            #(tn, fp, fn, tp) = confusion_matrix(y[test], y_pred).ravel()
            #print(i, (tn, fp, fn, tp))
            Performance_criteria.append(i)
            Performance_criteria.append(accuracy_score(y[test], y_pred))
            Performance_criteria.append(precision_score(y[test], y_pred))
            Performance_criteria.append(recall_score(y[test], y_pred))
            Performance_criteria.append(f1_score(y[test], y_pred))
            
            
            
            
            viz = RocCurveDisplay.from_estimator(
                clf,
                X[test],
                y[test],
                name="ROC fold {}".format(i),
                alpha=0.3,
                lw=1,
                ax=ax,
            )
            interp_tpr = np.interp(mean_fpr, viz.fpr, viz.tpr)
            interp_tpr[0] = 0.0
            tprs.append(interp_tpr)
            aucs.append(viz.roc_auc)
        
        
        ax.plot([0, 1], [0, 1], linestyle="--", lw=2, color="r", label="Chance", alpha=0.8)
        
        mean_tpr = np.mean(tprs, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr, mean_tpr)
        std_auc = np.std(aucs)
        ax.plot(
            mean_fpr,
            mean_tpr,
            color="b",
            label=r"Mean ROC (AUC = %0.2f $\pm$ %0.2f)" % (mean_auc, std_auc),
            lw=2,
            alpha=0.8,
        )
        
        std_tpr = np.std(tprs, axis=0)
        tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
        tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
        ax.fill_between(
            mean_fpr,
            tprs_lower,
            tprs_upper,
            color="grey",
            alpha=0.2,
            label=r"$\pm$ 1 std. dev.",
        )
        
        ax.set(
            xlim=[-0.05, 1.05],
            ylim=[-0.05, 1.05],
            title="Receiver operating characteristic example",
        )
        ax.legend(loc='upper left', bbox_to_anchor=(-.8, 1),
                  ncol=1, fancybox=True, shadow=True)
        
        
        plt.savefig(name)
        plt.show()
        
        Performance_criteria = np.array(Performance_criteria).reshape(kfold, 5)
        pd.DataFrame(Performance_criteria).to_csv(name1)
            

