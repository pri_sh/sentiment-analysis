from sklearn.feature_extraction.text import HashingVectorizer
import pandas as pd
import pickle
import numpy as np

with open('Data_After_Clean.pkl', 'rb') as f:
    mynewlist = pickle.load(f)


Tweets = []
for i in range(len(mynewlist)):
    Tweets.append(' '.join(mynewlist[i]))


n_features=160
vectorizer = HashingVectorizer(n_features=n_features)
vector = vectorizer.transform(Tweets)
A = vector.toarray()

name = 'Hashing_Vecotorize_'+str(n_features)
np.save(name, A)
