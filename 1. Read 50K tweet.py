import pandas as pd
from sklearn.utils import shuffle


DS_COLUMNS  = ["sentiment", "ids", "date", "flag", "user", "text"]
DS_ENCODING = "ISO-8859-1"

# Read Data: Sentiment140 Twitter
df = pd.read_csv('Data.csv',
                 encoding=DS_ENCODING, names=DS_COLUMNS,
                 dtype={'sentiment': 'int8'})

data = df.loc[:, 'text']

data_0 = df[df.loc[:, 'sentiment']==0]
data_1 = df[df.loc[:, 'sentiment']==4]

data_0 = shuffle(data_0)
data_1 = shuffle(data_1)

data_0 = data_0.reset_index()
data_1 = data_1.reset_index()

data_0.loc[:49999,:].to_csv('Label_0.csv')
data_1.loc[:49999,:].to_csv('Label_1.csv')



























































