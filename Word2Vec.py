import pandas as pd
import pickle
import numpy as np
from gensim.models import Word2Vec


with open('Data_After_Clean_Without_Stem.pkl', 'rb') as f:
    mynewlist = pickle.load(f)
    
vector_size = 20    
model = Word2Vec(mynewlist, min_count=10, vector_size=vector_size)

Word_Vec = []

for i in range(len(mynewlist)):
    if len(mynewlist[i])>0:
        count = 0
        A = np.zeros((1, vector_size))
        for  j in range(len(mynewlist[i])):        
            try:
                vector = model.wv[mynewlist[i][j]]
                A = A + vector
                count +=1
            except:
                pass
        
        for k in range(A.shape[1]):
            if np.isnan(A[0][k]):
                break
            else:
                Word_Vec.append(A/count)
                break


B = np.array(Word_Vec)
B = B.reshape(B.shape[0], vector_size)
name = 'word_vec_'+str(vector_size)+'.npy'
np.save(name, B)